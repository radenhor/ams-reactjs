import React, { Component } from 'react'
import {Header} from './Header'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useHistory,
    useLocation,
    Redirect
  } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {Admin} from "./Admin"
import Article from './Article';
import SweetAlert from 'react-bootstrap-sweetalert';
const axios = require('axios');
export default class Home extends Component{
    constructor(props){
      super(props);
      this.state = {
        isAdd : false,
        isEdit : false,
        isDelete : false,
        alert : null,
        article : {}
      }
    }
    onAdd = () => {
      this.setState({
        isAdd : true,
        isEdit : false, 
        isDelete : false,
        article : {}
      })
    }
    onEdit = (id) =>{
        axios.get('http://api-ams.me/v1/api/articles/'+id)
        .then((res) => {
          console.log(res)
           this.setState({
             isAdd : true,
             isEdit : true, 
             isDelete : false,
             article : {
               id : res.data.DATA.ID,
               title : res.data.DATA.TITLE,
               description : res.data.DATA.DESCRIPTION
             }
           })
        })
    }
    onSubmitArticle = (article) => {
        let {isAdd,isEdit} = this.state;
        if(isAdd&&isEdit){
          axios({
            url : 'http://api-ams.me/v1/api/articles/'+article.id,
            method : "PUT",
            data : {
              "TITLE" : article.title,
              "DESCRIPTION" : article.description
            }
          }).then((res) => {
             console.log(res)
             this.toast(res.data.MESSAGE,toast.TYPE.INFO)
             this.setState({
               isAdd : false,
               isEdit : false, 
               article : {}
             })
          })
        }else{
          axios({
            url : 'http://api-ams.me/v1/api/articles',
            method : "POST",
            data : {
              "TITLE" : article.title,
              "DESCRIPTION" : article.description
            }
          }).then((res) => {
            this.toast(res.data.MESSAGE,toast.TYPE.SUCCESS)
             console.log(res)
             this.setState({
               isAdd : false,
               isEdit : false, 
               article : {}
             })
          })
        }
    }
    hideAlert(id) {
      if(id==null){
        this.setState({
          alert: null
        });
      }else{
        this.setState({
          alert: null
        });
        axios({
          url : 'http://api-ams.me/v1/api/articles/'+id,
          method : "DELETE",
        }).then((res) => {
            this.toast(res.data.MESSAGE,toast.TYPE.ERROR)
            this.setState({
              isDelete : true
            })
            console.log(res)
        })
      }
      
    }
    onDelete = (id) => {
      const getAlert = () => (
        <SweetAlert 
          danger 
          showCancel
          confirmBtnText="Yes, delete it!"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="Are you sure?"
          onCancel = {()=>this.hideAlert(null)}
          onConfirm={() => this.hideAlert(id)}
        >
        </SweetAlert>
      );
      this.setState({
        alert: getAlert()
      });
    }
    onDeleted = () => {
      this.setState({
        isDelete : false
      })
    }
    NotFound = () =>{
      return (
        <div className="m-auto">
            <h1><h1>OPP</h1></h1>
            <Link to={"/"}>
                <Button className="btn btn-info">Go to Home page</Button>
            </Link>
            <h1>Page Not Found</h1>
        </div>
      )
    }
    Home = () => {
      return (
        <div className="m-auto">
            <h1><h1>សូមស្វា​គម​ន៍ ក្នុងការចូលមកគ្រប់គ្រងអត្ថបទ</h1></h1>
            <Link to={"/admin"}>
                <Button className="btn btn-info">Manage</Button>
            </Link>
            
        </div>
      )
    }
    toast = (msg,type) => toast(msg, { autoClose: 2000,type : type,position : toast.POSITION.BOTTOM_RIGHT });
    render(){
        return (
            <div className="vh-100" style={{
              backgroundImage : "url('https://thumbs-prod.si-cdn.com/PT-enupw3YGzvb_SHpFOxkWUI_M=/800x600/filters:no_upscale()/https://public-media.si-cdn.com/filer/70/e0/70e0989e-646e-4537-ae8e-7bbf863db2fd/ebjj1g.jpg')",
              backgroundRepeat : 'no-repeat',
              backgroundSize : 'cover'
            }}>
                <Header/>
                {this.state.alert}
                <ToastContainer autoClose={1000} />
                <Router>
                    <Switch>
                        <AdminRoute path={["/admin"]}>
                            {
                                !this.state.isAdd ? <Admin onAdd = {this.onAdd} deleted = {this.onDeleted} onEdit = {this.onEdit} onDelete = {this.onDelete} isDelete = {this.state.isDelete}/> : <Article article = {this.state.article} onSubmitArticle = {this.onSubmitArticle}/>
                            }
                        </AdminRoute>
                        <Route exact path={["/","/home"]} render={()=><this.Home/>} />
                        <Route path="/login" component={()=> <LoginPage/> } />
                        <Route path="/detail/:id" component={ ViewArticle } />
                        <Route path="*" component={()=><this.NotFound></this.NotFound>}/>
                       
                    </Switch>
                </Router>
            </div>
    )
    }
}

const fakeAuth = {
    isAuthenticated: false,
    authenticate(cb) {
      fakeAuth.isAuthenticated = true;
      setTimeout(cb, 100); // fake async
    },
    signout(cb) {
      fakeAuth.isAuthenticated = false;
      setTimeout(cb, 100);
    }
};

function LoginPage(){
  let history = useHistory();
  let location = useLocation();

  let { from } = location.state || { from: { pathname: "/" } };
  let login = () => {
    
    fakeAuth.authenticate(() => {
      history.replace({
        pathname : from.pathname
      });
    });
  };
  return (
    <div>
      <p>You must log in to view the page at {from.pathname}</p>
      <Button onClick={login}>Log in</Button>
    </div>
  );
}

function AdminRoute({ children, ...rest }){
    return (
        <Route
          {...rest}
          render={({ location }) =>
            fakeAuth.isAuthenticated ? (
              children
            ) : (
              <Redirect
                to={{
                  pathname: "/login",
                  state: { from: location }
                }}
              />
            )
          }
        />
    );
}

class ViewArticle extends Component {
  constructor(props){
    super(props)
    this.state = {
      article : {},
      load : true,
      error : false
    }
  }
  componentDidMount(){
    axios.get('http://api-ams.me/v1/api/articles/'+this.props.match.params.id)
    .then((res) => {
        this.setState({
          article : {
            id : res.data.DATA.ID,
            title : res.data.DATA.TITLE,
            description : res.data.DATA.DESCRIPTION
          },
          load : false
        })
    })
    .catch(err => {
      this.setState({
        load : true,
        error : true
      })
    })
  }

  render(){
      return  this.state.load 
              ?  this.state.error ? <div>Error can't find article</div> : <div>Fetching.....</div>
              :  (<div>
                    <h1>{this.state.article.title}</h1>
                    <p>{this.state.article.description}</p>
                  </div>);    
  }
}