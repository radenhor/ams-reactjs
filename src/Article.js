import React, { Component } from 'react'
import {Button,Form} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

export default class Article extends Component {
    constructor(props){
        super(props)
        this.state = {
            id : '',
            title : '',
            description : ''
        }
    }
    componentDidMount(){
        if(this.props.article){
            let {id,title,description} = this.props.article
            this.setState({
                id : id,
                title : title,
                description : description
            })
        }
    }
    onSubmitArticle  = (e) => {
        this.props.onSubmitArticle(this.state)
        e.preventDefault()
    }
    onChange = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-4">
                        <Form onSubmit = {this.onSubmitArticle}>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Title</Form.Label>
                                <Form.Control type="text" name = "title" onChange = {(e)=>this.onChange(e)} value = {this.state.title} placeholder="Enter title"/>
                            </Form.Group>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" placeholder="Enter description" name = "description" onChange = {(e)=>this.onChange(e)} value = {this.state.description}/>
                            </Form.Group> 
                            <Button variant="primary" type="submit">
                                បញ្ចូល
                            </Button>
                        </Form>
                    </div>
                </div>
                
            </div>
        )
    }
}
