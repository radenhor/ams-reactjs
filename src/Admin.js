import React, { Component } from 'react'
import {Table,Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Link} from "react-router-dom";
import { thisExpression } from '@babel/types';
const axios = require('axios');
export  class Admin extends Component {
    constructor(props){
        super(props)
        this.state = {
            data : []
        }
    }
    async getArticle(id) {
        try {
            let url  = ""
            if(id==null){
                url = 'http://api-ams.me/v1/api/articles?page=1&limit=15'
            }else{
                url  = 'http://api-ams.me/v1/api/articles/'+id;
            }
            const response = await axios.get(url);
            return response;
        } catch (error) {
            console.error(error);
            }
    }   
    componentDidMount(){
        this.getArticle(null).then((res)=>{
            this.setState({
                data : res.data.DATA
            })
        })
    }
    componentDidUpdate(){
        if(this.props.isDelete){
            this.getArticle(null).then((res)=>{
                this.props.deleted()
                this.setState({
                    data : res.data.DATA
                })
            })
        }
    }
    ViewArticle  = () =>{
        console.log(this.props.match.params.id);
        return <h1>Hello</h1>
    }
    render() {
        
        return (
            <div>
                <div className="m-3">
                    <h1>ការរៀនរីអែក</h1>
                    <Button className="btn btn-primary" onClick = { ()=> this.props.onAdd()}>Add</Button>
                    <h1>បញ្ជីអត្ថបទ</h1>
                </div>
                <Table striped bordered hover variant="dark">
                    <thead>
                        <tr>
                        <th>#ID</th>
                        <th>TITLE</th>
                        <th>DESCRIPTION</th>
                        <th>CREATED_DATE</th>   
                        <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.data.map((data)=>
                            <tr key={data.ID}>
                                <td>{data.ID}</td>
                                <td>{data.TITLE}</td>
                                <td>{data.DESCRIPTION}</td>
                                <td>{data.CREATED_DATE}</td>
                                <td>
                                    <Link to={"/detail/"+data.ID}>
                                            <Button className="btn btn-info">View</Button>
                                    </Link>
                                    <Button onClick={()=> this.props.onEdit(data.ID)} className="btn btn-primary mx-2">Edit</Button>
                                    <Button className="btn btn-danger" onClick = {()=>this.props.onDelete(data.ID)}>Delete</Button>
                                </td>
                            </tr>
                        )
                    }   
                    </tbody>
                </Table>
            </div>
          )
    }

}
